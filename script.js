console.warn(`JSON Introduction`);

// json -- JS Obj is not the same as JSON. JSON uses double quote for
// property names

/*json object
{
	"city": "Quezon City",
	"country": "Philippines"
}*/

// josn array
/*"cities":[
	{"city": "Quezon City", "country": "Philippines"},
	{"city": "Manila", "country": "Philippines"},
	{"city": "Caloocan", "country": "Philippines"}
]*/

// json method -- json obj contains methods for parsing and
// converting data into a stringfield json

// converting data into stringfield json

let batches = [
	{
		batchName: `Batch 176`
	},
	{
		batchName: `Batch 177`
	}
];

console.log(batches);
console.warn(JSON.stringify(batches));

let data = JSON.stringify({
	name: `John`,
	age: 25,
	address: {
		city: `Quezon City`,
		country: `Philippines`
	}
});

console.warn(data);

/*let name = prompt(`First name`);
let lname = prompt(`Last name`);
let age = Number(prompt(`Age`));
let address = {
	city: prompt(`Your city`),
	country: prompt(`Your country`)
};

let obj1 = JSON.stringify({
	name: name,
	"last name": lname, //spaces are not allowed for properties on json, if not enclosed with doubke quotes
	age: age,
	address: address
});

console.warn(obj1);*/

let obj2 = `[
	{
		"batchname": 176
	},
	{
		"batchname": 177
	}
]`;

console.warn(obj2);
console.warn(JSON.parse(obj2));
console.warn(JSON.stringify(obj2));

let obj3 = `{
	"name": "John",
	"age": 25,
	"address": {
		"city": "QC",
		"country": "Philippines"
	}
}`;

console.warn(obj3);
console.warn(JSON.parse(obj3));
console.warn(JSON.stringify(obj3))

/*JSON.parse() - converting a JSON string into an object

JSON.stringify() - creates a JSON string*/